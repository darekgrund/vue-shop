new Vue({
    el: '#vue-app-refactor',

    data: {
        showBasket: false,
        itemMoveToBasketAnimation: [],
        basket: [],
        errorMsg: '',
        products: _products,
        newProduct: {name: '', desc: '', price: '', img: ''}
    },

    methods: {
        addToBasket(product) {
            this.basket.push(product);

            // Basket animation
            this.itemMoveToBasketAnimation.push('+1');
            setTimeout(() => {
                this.itemMoveToBasketAnimation.shift();
            }, 1000);
        },

        removeFromBasket(index) {
            this.basket.splice(index, 1);
        },

        createNewProduct() {
            let isNewProductValid = this._validateProduct();
            if (isNewProductValid) {
                this.newProduct.img === '' ?
                    this.newProduct.img = 'http://food.fnr.sndimg.com/content/dam/images/food/fullset/2013/11/26/1/FNK_rainbow-spectrum_s4x3.jpg.rend.hgtvcom.616.462.suffix/1386172541852.jpeg' : false;
                this.products.push(this.newProduct);
                this.newProduct = {name: '', desc: '', price: '', img: ''}
            }
        },

        _validateProduct() {
            let p = this.newProduct;
            this.errorMsg = '';
            this.errorMsg === '' && (p.name.length < 2 || p.name.length > 14) ?
                this.errorMsg = 'Name of product must be 2 to 14 characters long.' : false;
            this.errorMsg === '' && (p.desc.length < 5 || p.desc.length > 30) ?
                this.errorMsg = 'Description of product must be 5 to 30 characters long.' : false;
            this.errorMsg === '' && (p.price < 0.5 || p.price > 10) ?
                this.errorMsg = 'Price must be between 0.5PLN and 10PLN.' : false;
            return this.errorMsg === '';
        }
    },

    computed: {
        totalPrice() {
            return this.basket.reduce(function (prevVal, product) {
                return prevVal + product.price;
            }, 0).toFixed(2);
        }
    }
});