const _products = [
    {
        name: 'apple',
        desc: 'just an simple expensive apple',
        price: 1.99,
        img: 'img/apple.jpg'
    },
    {
        name: 'pickle',
        desc: 'just an simple expensive pickle',
        price: 2.99,
        img: 'img/pickle.jpg'
    },
    {
        name: 'tomato',
        desc: 'just an simple expensive tomato',
        price: 3.49,
        img: 'img/tomato.jpg'
    }
];